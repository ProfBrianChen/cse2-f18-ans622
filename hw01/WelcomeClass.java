//Anshika Singh 
// 09/02/2018
//CSE 02 Carr TR



//////////////

//// CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args[]){
    /// prints Welcome with box around it to terminal window
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
     // printing username with symbols around it
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("A-N--S--6--2--2 |");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
    
    
  }
}