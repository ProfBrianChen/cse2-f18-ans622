
import java.util.Scanner;

public class Hw05 {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		// declaring the variables
		int onePairCounter = 0;
		int twoPairCounter = 0;
		int threeOfaKind = 0;
		int fourOfaKind = 0;
		double probabilityOfFourOfaKind = 0;
		double probabilityOfThreeOfaKind = 0;
		double probabilityOfTwoPair = 0;
		double probabilityOfaOnePair = 0;

		System.out.println("Enter number of loops:");
		// ...we'll trust the user
		int loops = myScanner.nextInt();
		for (int i = 1; i <= loops; i++) {
			// Generate 5 distinct cards
			int card1 = (int) (Math.random() * (52));

			int card2 = (int) (Math.random() * (52));

			int card3 = (int) (Math.random() * (52));

			int card4 = (int) (Math.random() * (52));

			int card5 = (int) (Math.random() * (52));

			// make sure cards do not overlap
			while (card2 == card1) {

				card2 = (int) (Math.random() * (52));
			}
			while (card3 == card2 || card3 == card1) {
				card3 = (int) (Math.random() * (52));
			}
			while (card4 == card3 || card4 == card2 || card4 == card1) {
				card4 = (int) (Math.random() * (52));
			}
			while (card5 == card4 || card5 == card1 || card5 == card2 || card5 == card3) {
				card5 = (int) (Math.random() * (52));
			}

			// mod down the cards based on different suits
			card1 = card1 % 13;

			card2 = card2 % 13;

			card3 = card3 % 13;

			card4 = card4 % 13;

			card5 = card5 % 13;

			// using if statements to write out all possibilities
	

			if ((card1 == card2 && card1 == card3 && card1 == card4)
					|| (card1 == card2 && card1 == card3 && card1 == card5)
					|| (card1 == card2 && card1 == card4 && card1 == card5)
					|| (card1 == card3 && card1 == card2 && card1 == card4)
					|| (card1 == card3 && card1 == card4 && card1 == card5)
					|| (card1 == card4 && card1 == card3 && card1 == card2)
					|| (card1 == card4 && card1 == card5 && card1 == card2)
					|| (card1 == card5 && card1 == card2 && card1 == card3)
					|| (card1 == card5 && card1 == card4 && card1 == card2)
					|| (card1 == card5 && card1 == card4 && card1 == card3)) {
				fourOfaKind++;
			} else {
				if ((card1 == card2 && card1 == card3) ||

						(card1 == card2 && card1 == card4) ||

						(card1 == card2 && card1 == card5) ||

						(card1 == card3 && card1 == card2) ||

						(card1 == card3 && card1 == card4) ||

						(card1 == card3 && card1 == card5) ||

						(card1 == card4 && card1 == card2) ||

						(card1 == card4 && card1 == card5)) {
					threeOfaKind++;
				} else {
					if ((card1 == card2 && card3 == card4 || card1 == card2 && card3 == card5
							|| card1 == card2 && card4 == card5) ||

							(card1 == card3 && card2 == card4 || card1 == card3 && card2 == card5
									|| card1 == card3 && card3 == card4)
							||

							(card1 == card4 && card2 == card3 || card1 == card4 && card2 == card5
									|| card1 == card4 && card4 == card5)
							||

							(card1 == card5 && card2 == card4 || card1 == card5 && card2 == card4
									|| card1 == card5 && card3 == card4)
							||

							(card2 == card3 && card4 == card5 || card2 == card3 && card1 == card4
									|| card2 == card3 && card1 == card5)
							||

							(card2 == card4 && card1 == card5 || card2 == card4 && card3 == card5
									|| card2 == card4 && card1 == card3)
							||

							(card2 == card5 && card3 == card4 || card2 == card5 && card1 == card3
									|| card2 == card5 && card1 == card4)) {
						twoPairCounter++;
					} else {
						if (card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5 || card2 == card3
								|| card2 == card4 || card2 == card5 || card3 == card4 || card3 == card5
								|| card4 == card5) {
							onePairCounter++;
						}
					}
				}

			}
		}

		// calculating possibilities 
		probabilityOfFourOfaKind = (double) fourOfaKind / (double) loops;
		probabilityOfFourOfaKind = (double) Math.round(probabilityOfFourOfaKind * 1000d) / 1000d;

		probabilityOfThreeOfaKind = (double) threeOfaKind / (double) loops;
		probabilityOfThreeOfaKind = (double) Math.round(probabilityOfThreeOfaKind * 1000d) / 1000d;

		probabilityOfTwoPair = (double) twoPairCounter / (double) loops;
		probabilityOfTwoPair = (double) Math.round(probabilityOfTwoPair * 1000d) / 1000d;

		probabilityOfaOnePair = (double) onePairCounter / (double) loops;
		probabilityOfaOnePair = (double) Math.round(probabilityOfaOnePair * 1000d) / 1000d;

		// printing out the probability

		System.out.println("The numeber of loops are: " + loops);
		System.out.println("The probability of four of a kind is: " + probabilityOfFourOfaKind);
		System.out.println("The probability of three of a kind is: " + probabilityOfThreeOfaKind);
		System.out.println("The probability of a two pair is: " + probabilityOfTwoPair);
		System.out.println("The probability of a one pair is: " + probabilityOfaOnePair);

	}
}
