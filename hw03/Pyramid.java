//in order for scanner to work
import java.util.Scanner;

//define the class
public class Pyramid{

//adding main method
public static void main(String[] args) {

// allowing scanner to take the input
Scanner myScanner = new Scanner ( System.in );
  
 //length of the square
System.out.print("The square side of the pyramid is (input length): ");
  
//accept the input 
int lengthPyramid = myScanner.nextInt();
 
//height of the pyramid 
System.out.print("The height of the pyramid is (input height): ");

//accept input
 int heightPyramid = myScanner.nextInt();

//printing the output 
int areaPyramid; //total area of the pyramid
//for the area
areaPyramid = (lengthPyramid * heightPyramid * lengthPyramid * 1/3 );
System.out.print("The volume inside the pyramid is : " + areaPyramid );
}
}
  