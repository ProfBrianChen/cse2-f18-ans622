///Anshika Singh
///Carr TR
/// CSE 002

//in order for scanner to work
import java.util.Scanner;

//define the class
public class Convert{

//adding main method
public static void main(String[] args) {

// allowing scanner to take the input
Scanner myScanner = new Scanner ( System.in );
  
// the area affected in acres 
System.out.print("Enter the affected area in acres: ");

// accept the input
double areaAcres = myScanner.nextDouble();

// promt the user to accept the affected ares
System.out.print("Enter the rainfall in the affectd area: " );
double areaAffected = myScanner.nextDouble();
  
// printing output
// variables
double acreInchToGallons = 3.6826598951644E-5; // conversion 
double gallonsToCubicMiles = 1101117147428.6; //conversion
double totalAffectedInAcreInches;
double totalAffectedInGallons;
double finalInCubicMiles;
//equations
totalAffectedInAcreInches = (areaAffected * areaAcres);
totalAffectedInGallons = (totalAffectedInAcreInches / acreInchToGallons);
finalInCubicMiles = (totalAffectedInGallons / gallonsToCubicMiles);
// actual print
System.out.print("The quantity of the rain in cubic miles is " + finalInCubicMiles);

  }
  
}