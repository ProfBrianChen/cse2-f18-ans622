/// Anshika Singh
/// 09/06/2018
/// CSE 002 Carr TR
// define a class
public class Cyclometer {
  
// add main method
   	public static void main(String[] args) {

      
//Declaring the variables used in this lab
      int secsTrip1=480;  // The amount of seconds it takes for Trip 1
      int secsTrip2=3220;  // The amount of seconds it takes for trip 2
		  int countsTrip1=1561;  // The amount of rotations for Trip 1
		  int countsTrip2=9037; // The amount of rotations for Trip 2
      
// Assigning data values to the variables above
      double wheelDiameter=27.0,  // the diamter of the bicycle's wheel
     	PI=3.14159, // the numerical value of pi
    	feetPerMile=5280,  // the conversion of miles and feet
    	inchesPerFoot=12,   // the conversion of inches and feet
    	secondsPerMinute=60;  // the conversion of seconds and minutes
	    double distanceTrip1, distanceTrip2,totalDistance;  // the total distances of the three trips 
      
// Printing the data numbers that have been stored
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      
 // the calculations of the trips using the data stored above
     distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
      
// printing out the distances 
     
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");

      
        
      }
      

}