// Anshika Singh
// 09/06/18
// CSE 002 TR Carr

// define a class
public class Arithmetic{
 
  // add main method 
   public static void main(String[] args) {
  
 // assign variables and its numerical value
int numPants = 3; // number of pants bought
double pantsPrice = 34.98; // cost per pants
int numShirts = 2; // number of shirt bought
double shirtPrice = 24.99; // price per shirt
int numBelts = 1; // number of belts for purchase
double beltPrice = 33.99; // cost per belt
double paSalesTax = 0.06; // sales tax on purchase
double totalCostOfPants;   //total cost of pants
double totalCostOfShirts;   //total cost of shirts
double totalCostOfBelts;   //total cost of belts
double totalCostOfShopping; // total cost without tax
double totalCostofShoppingt; // total cost with tax
double taxOfTheSale; // the tax amount from the total cost

     
// access the variable data
 totalCostOfPants = (numPants * pantsPrice); //numerical value of the total cost of pants 
 totalCostOfShirts = (numShirts * shirtPrice); // numerical value of the total cost of shirts
 totalCostOfBelts = (numBelts * beltPrice); // numerical value of the total cost of belts
 totalCostOfShopping = (totalCostOfPants + totalCostOfBelts + totalCostOfShirts); // numerical value of the total cost of shopping totalCostofShopping = (totalCostOfPants + totalCostOfBelts + totalCostOfShirts); // numerical value of the total cost of shopping
 taxOfTheSale = (totalCostOfShopping * paSalesTax); // the sales tax of the total cost
 totalCostofShoppingt = (totalCostOfShopping + taxOfTheSale); // total cost with the tax

  //printing the the variables into words and actual numerical values
     System.out.println("The total cost of the transaction before tax is " + "$" + Math.round(totalCostOfShopping * 100.0) / 100.0 + "." );
     System.out.println("The total cost of the sales tax is " + "$" + Math.round(taxOfTheSale * 100.0) / 100.0  + ".");
     System.out.println("The total cost of the transaction with taxes is " + "$" + Math.round(totalCostofShoppingt * 100.0) / 100.0 + "." );
 
   
    
   
   }
}
   
   
   