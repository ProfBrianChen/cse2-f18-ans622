///Anshika Singh
/// Carr TR 
/// Cse 002

// in order for scanner command to work
import java.util.Scanner;

//define a class
public class Check{ 

// adding main method
public static void main(String[] args) {

//allowing scanner to take input
Scanner myScanner = new Scanner( System.in );

// orginal cost of the check 
System.out.print("Enter the original cost of the check in the form xx.xx: ");
  
//accept user input
double checkCost = myScanner.nextDouble();

//prompting user to accept the tip perecentage they would like to pay
System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
double tipPercent = myScanner.nextDouble();

//convertt to percentage                
tipPercent /= 100;

// number of ways the cost would be split
System.out.print("Enter the number of people who went out to dinner: ");
int numPeople = myScanner.nextInt();
  
//printing the output
double totalCost;
double costPerPerson;
int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits
//to the right of the decimal point 
//for the cost$ 
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson;
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
dimes=(int)(costPerPerson * 10) % 10;
pennies=(int)(costPerPerson * 100) % 10;
System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);



  
                


 



 




  








}
  }