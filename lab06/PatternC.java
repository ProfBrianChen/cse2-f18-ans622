// Anshika Singh
// 09/06/18
// CSE 002 TR Carr

import java.util.Scanner;

public class PatternC {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		boolean looping = true;
		int numRows = 0;

		while (looping) {
			System.out.println("Please enter number between 1 and 10: ");
			boolean correct = myScanner.hasNextInt();
			if (correct) {
				numRows = myScanner.nextInt();
				if (numRows > 0 && numRows < 11) {
					looping = false;
				}
			} else {
				String junkword = myScanner.next();

			}
		}
		for (int row = 1; row <= numRows; row++) {
			for (int col = numRows; col >= 1; col--) {
				if(col > row) {
					System.out.print(" ");
				}
				else {
					System.out.print(col);	
				}
				
			}
			System.out.println();
		}

	}

}
